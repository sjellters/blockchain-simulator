package blockchain.client;

import blockchain.messaging.Message;

public class MinerActions {
    public static void onVerifySHA(String word, String key, int zerosLength) {
        Miner.isBusy.set(false);

        Message message = new Message();

        if (SHA1.isValid(word, key, zerosLength)) {
            message.setAction("SHA-VERIFIED");
        } else {
            message.setAction("SHA-NOT-VERIFIED");
        }

        Miner.sendMessage(message.getMessage());
    }

    public static void onNewWord(String word, int zerosLength) {
        Miner.isBusy.set(true);
        Message message = SHA1.getDigest(word, zerosLength);
        message.setAction("SHA-FOUND");

        Miner.isBusy.set(false);
        Miner.sendMessage(message.getMessage());
    }
}
