package blockchain.server.miner;

import blockchain.messaging.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

public class Miner {
    private Socket socket;
    private String id;
    private PrintWriter outputMessageStream;
    private AtomicBoolean isConnected;

    public Miner(Socket socket) {
        this.socket = socket;
        this.id = UUID.randomUUID().toString();
        try {
            this.outputMessageStream = new PrintWriter(socket.getOutputStream(), true);
            this.isConnected = new AtomicBoolean(true);
            new InputStreamReaderThread().start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void sendMessage(String message) {
        outputMessageStream.println(message);
    }

    public class InputStreamReaderThread extends Thread {

        @Override
        public void run() {
            super.run();
            try {
                BufferedReader inputStreamReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String messageLine;

                while ((messageLine = inputStreamReader.readLine()) != null && isConnected.get()) {
                    Message messageOnReceive = new Message(messageLine);

                    switch (messageOnReceive.getAction()) {
                        case "SHA-FOUND":
                            MinerActions.onSHAFound(messageOnReceive);
                            break;
                        case "SHA-VERIFIED":
                            MinerActions.onSHAVerified();
                            break;
                        case "SHA-NOT-VERIFIED":
                            MinerActions.onSHANotVerified();
                            break;
                        default:
                            break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
