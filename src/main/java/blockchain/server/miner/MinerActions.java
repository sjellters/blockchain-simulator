package blockchain.server.miner;

import blockchain.messaging.Message;
import blockchain.server.BlockchainServer;
import blockchain.server.ServerActions;

public class MinerActions {
    public static void onSHAFound(Message message) {
        BlockchainServer.setCurrentMinerInfo(String.format("%s|%s|%s|%s|%s|%s", message.getWord(), message.getKey(), message.getZerosLength(), message.getTime(), message.getMinerIdentifier(), message.getSha()));

        ServerActions.verifySHA(message.getWord(), message.getKey());
    }

    public static void onSHAVerified() {
        ServerActions.SHAVerified();
    }

    public static void onSHANotVerified() {
        ServerActions.SHANotVerified();
    }
}
