package blockchain.server.queue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

public class WordsQueue {
    public static Queue<String> wordsQueue = new LinkedList<>();

    public static void read() {
        try {
            URL fileUrl = WordsQueue.class.getClassLoader().getResource("words-list.txt");
            File file = new File(Objects.requireNonNull(fileUrl).getFile());
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;

            while ((line = bufferedReader.readLine()) != null) {
                wordsQueue.offer(line);
            }
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static String getWord() {
        return wordsQueue.poll();
    }

    public static void enqueueWord(String word) {
        wordsQueue.offer(word);
    }
}
