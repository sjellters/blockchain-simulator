package blockchain.client;

import blockchain.messaging.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

public class Miner {
    public static AtomicBoolean isBusy = new AtomicBoolean();
    static String id;
    static Socket socket;
    static PrintWriter outputMessageStream;

    public static void startMiner() {
        try {
            socket = new Socket("127.0.0.1", 3000);
            outputMessageStream = new PrintWriter(socket.getOutputStream(), true);
            new InputStreamReaderThread().start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        startMiner();
    }

    public static String getId() {
        return id;
    }

    public static void setId(String id) {
        Miner.id = id;
    }

    public static void sendMessage(String message) {
        outputMessageStream.println(message);
    }

    public static class InputStreamReaderThread extends Thread {

        @Override
        public void run() {
            super.run();
            try {
                BufferedReader inputStreamReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String messageLine;

                while ((messageLine = inputStreamReader.readLine()) != null) {
                    Message message = new Message(messageLine);

                    switch (message.getAction()) {
                        case "ASSIGN":
                            setId(message.getMinerIdentifier());
                            break;
                        case "NEW-WORD":
                            MinerActions.onNewWord(message.getWord(), message.getZerosLength());
                            break;
                        case "VERIFY-SHA":
                            MinerActions.onVerifySHA(message.getWord(), message.getKey(), message.getZerosLength());
                            break;
                        default:
                            break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
