package blockchain.server;

import blockchain.messaging.Message;
import blockchain.server.miner.Miner;
import blockchain.server.queue.WordsQueue;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class BlockchainServer {
    private static final List<Miner> miners = new ArrayList<>();
    private static final AtomicBoolean isActive = new AtomicBoolean();
    private static final AtomicBoolean isVerifying = new AtomicBoolean();
    private static final Integer numberOfMiners = 4;
    private static final AtomicInteger currentVerifiedCount = new AtomicInteger(0);
    private static ServerSocket serverSocket;
    private static String currentWord;
    private static String currentMinerInfo;

    public static AtomicBoolean getIsVerifying() {
        return isVerifying;
    }

    public static Integer getNumberOfMiners() {
        return numberOfMiners;
    }

    public static String getCurrentMinerInfo() {
        return currentMinerInfo;
    }

    public static void setCurrentMinerInfo(String currentMinerInfo) {
        BlockchainServer.currentMinerInfo = currentMinerInfo;
    }

    public static AtomicInteger getCurrentVerifiedCount() {
        return currentVerifiedCount;
    }

    public static String getCurrentWord() {
        return currentWord;
    }

    public static void setCurrentWord(String currentWord) {
        BlockchainServer.currentWord = currentWord;
    }

    public static void startServerSocket() {
        if (serverSocket == null) {
            try {
                serverSocket = new ServerSocket(3000);
                isActive.set(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void main(String[] args) {
        startServerSocket();
        WordsQueue.read();
        new WaitForMinersThread().start();
    }

    public static List<Miner> getMiners() {
        return miners;
    }

    public static class WaitForMinersThread extends Thread {
        @Override
        public void run() {
            super.run();

            while (isActive.get()) {
                try {
                    Socket minerSocket = serverSocket.accept();
                    Miner miner = new Miner(minerSocket);
                    miner.setId(UUID.randomUUID().toString());

                    Message message = new Message();

                    message.setAction("ASSIGN");
                    message.setMinerIdentifier(miner.getId());

                    miner.sendMessage(message.getMessage());
                    miners.add(new Miner(minerSocket));

                    if (miners.size() == numberOfMiners) {
                        ServerActions.sendNewWord();
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
