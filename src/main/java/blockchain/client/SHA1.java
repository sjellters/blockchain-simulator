package blockchain.client;

import blockchain.messaging.Message;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

public class SHA1 {
    public static Message getDigest(String data, int zerosLength) {
        String zerosString = "0".repeat(zerosLength);

        Message message = new Message();

        while (Miner.isBusy.get()) {
            String key = UUID.randomUUID().toString();
            String _data = String.format("%s %s", data, key);

            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-1");
                digest.reset();
                digest.update(_data.getBytes(StandardCharsets.UTF_8));
                String sha1 = String.format("%040x", new BigInteger(1, digest.digest()));

                if (sha1.startsWith(zerosString)) {
                    message.setWord(data);
                    message.setZerosLength(zerosLength);
                    message.setKey(key);
                    message.setMinerIdentifier(Miner.getId());
                    message.setTime("0");
                    message.setSha(sha1);

                    return message;
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        return message;
    }

//    static class DigestSHA {
//        String key;
//        String data;
//        String sha;
//    }
//
//    public static String sequentialGetDigest(AtomicBoolean notFound, String zerosString, String _data) {
//        while(notFound.get()) {
//            try {
//                MessageDigest digest = MessageDigest.getInstance("SHA-1");
//                digest.reset();
//                digest.update(_data.getBytes(StandardCharsets.UTF_8));
//                String sha1 = String.format("%040x", new BigInteger(1, digest.digest()));
//
//                if (sha1.startsWith(zerosString)) {
//                    return message;
//                }
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    public static boolean isValid(String data, String key, int zerosLength) {
        String _data = String.format("%s %s", data, key);
        String zerosString = "0".repeat(zerosLength);

        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            digest.update(_data.getBytes(StandardCharsets.UTF_8));
            String sha1 = String.format("%040x", new BigInteger(1, digest.digest()));

            if (sha1.startsWith(zerosString)) {
                return true;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return false;
    }
}