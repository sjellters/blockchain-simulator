package blockchain.messaging;

public class Message {
    String action;
    String word;
    String key;
    int zerosLength;
    String time;
    String minerIdentifier;
    String sha;

    public Message() {
    }

    public Message(String message) {
        String[] splitMessage = message.split("\\|");

        action = splitMessage[0];
        word = splitMessage[1];
        key = splitMessage[2];
        zerosLength = Integer.parseInt(splitMessage[3]);
        time = splitMessage[4];
        minerIdentifier = splitMessage[5];
        sha = splitMessage[6];
    }

    public String getSha() {
        return sha;
    }

    public void setSha(String sha1) {
        this.sha = sha1;
    }

    public String getMessage() {
        return String.format("%s|%s|%s|%s|%s|%s|%s", action, word, key, zerosLength, time, minerIdentifier, sha);
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getZerosLength() {
        return zerosLength;
    }

    public void setZerosLength(int zerosLength) {
        this.zerosLength = zerosLength;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMinerIdentifier() {
        return minerIdentifier;
    }

    public void setMinerIdentifier(String minerIdentifier) {
        this.minerIdentifier = minerIdentifier;
    }
}
