package blockchain.server;

import blockchain.messaging.Message;
import blockchain.server.miner.Miner;
import blockchain.server.queue.WordsQueue;

public class ServerActions {
    public static void sendToAllMiners(Message message) {
        for (Miner miner : BlockchainServer.getMiners()) {
            miner.sendMessage(message.getMessage());
        }
    }

    public static void sendNewWord() {
        BlockchainServer.getCurrentVerifiedCount().set(0);
        BlockchainServer.setCurrentWord(WordsQueue.getWord());

        if (BlockchainServer.getCurrentWord() != null) {
            Message message = new Message();

            message.setAction("NEW-WORD");
            message.setWord(BlockchainServer.getCurrentWord());
            //Current Zeros
            message.setZerosLength(3);

            sendToAllMiners(message);
        }
    }

    public static void verifySHA(String word, String key) {
        Message message = new Message();

        message.setAction("VERIFY-SHA");
        message.setWord(word);
        message.setKey(key);

        BlockchainServer.getIsVerifying().set(true);

        sendToAllMiners(message);
    }

    public static void SHAVerified() {
        if (BlockchainServer.getIsVerifying().get()) {
            BlockchainServer.getCurrentVerifiedCount().addAndGet(1);

            if (BlockchainServer.getCurrentVerifiedCount().get() == BlockchainServer.getNumberOfMiners()) {
                System.out.println("SHA-FOUND AND VERIFIED: " + BlockchainServer.getCurrentMinerInfo());
                sendNewWord();
            }
        }
    }

    public static void SHANotVerified() {
        BlockchainServer.getIsVerifying().set(false);
        WordsQueue.enqueueWord(BlockchainServer.getCurrentWord());
        sendNewWord();
    }
}
